---Select * from PortfolioProject..['covid-deaths$']
--order by 3,4

--select location , date ,total_cases_per_million, new_cases, total_deaths, population 
--from PortfolioProject..['covid-deaths$']
--order by 1,2


-- Looking at Total cases vs total deaths

select location , date ,total_cases_per_million, total_deaths_per_million, (total_deaths_per_million/total_cases_per_million)*100 as DeathPercentage
from PortfolioProject..['covid-deaths$']
Where location like '%canada%'
order by 1,2


--Total cases vs population
select location , date ,total_cases_per_million, total_deaths,population ,(total_cases_per_million/population)*100 as PercentPopulation
from PortfolioProject..['covid-deaths$']
Where location like '%canada%'
order by 1,2

--Countries with highest infection rate compared to population
select location,MAX(total_cases_per_million) as HighestInfectionCount,population ,total_cases,MAX((total_cases_per_million/population))*100 as PercentPopulationInfected
from PortfolioProject..['covid-deaths$']
Group by location,population
order by PercentPopulationInfected desc

--SHowing continents with highest death count per population
select continent, MAX(cast(total_deaths as int)) as TotalDeathCount
from PortfolioProject..['covid-deaths$']
where continent is not null
Group by continent
order by TotalDeathCount desc

--Global Numbers
select date ,SUM(new_cases) as TotalCases, SUM(cast(new_deaths as int)) as TotalDeaths, SUM(cast(new_deaths as int)) /SUM(new_cases) as DeathPercentage
from PortfolioProject..['covid-deaths$']
Where continent is not null
group by date 
order by 1,2

--Total population VS vaccination(USING CTE(COMMON TABLE EXPRESSION))

WITH PopVsVac (Continent,Location,Date,Population,New_Vaccinations,RollingPeopleVaccinated)
as
(
SELECT d.continent, d.location, d.date, d.population, cast(v.new_vaccinations as int), 
SUM((cast(v.new_vaccinations as bigint))) OVER (Partition by d.Location order by d.location, d.date) as RollingPeopleVaccinated
From PortfolioProject..['covid-deaths$'] D
JOIN PortfolioProject..['covid-vaccination$'] V
	ON d.location = v.location
	and d.date = v.date
where d.continent is not null
)
Select *,(RollingPeopleVaccinated/Population)*100 as VaccinatedPeoplePercentage
from PopVsVac


--Creating view to store data for vizualizations

Create View PercentPopulationVaccinated as
SELECT d.continent, d.location, d.date, d.population,v.new_vaccinations, 
SUM((cast(v.new_vaccinations as bigint))) OVER (Partition by d.Location order by d.location, d.date) as RollingPeopleVaccinated
From PortfolioProject..['covid-deaths$'] D
JOIN PortfolioProject..['covid-vaccination$'] V
	ON d.location = v.location
	and d.date = v.date
where d.continent is not null

Select * 
From PercentPopulationVaccinated